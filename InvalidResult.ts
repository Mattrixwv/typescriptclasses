//typescriptClasses/InvalidResult.ts
//Matthew Ellison
// Created: 10-18-20
//Modified: 10-18-20
//This file contains a class that is used to time the execution time of other programs
/*
Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


export class InvalidResult{
	private message: string;
	public constructor(message: string){
		this.message = message;
	
	}
	public getMessage(): string{
		return this.message;
	}
}
