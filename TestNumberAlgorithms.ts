//typescriptClasses/TestNumberAlgorithms.ts
//Matthew Ellison
// Created: 07-13-21
//Modified: 07-13-21
//Tests for the number algorithms
/*
Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import assert = require("assert");
import { arrayEquals } from "./ArrayAlgorithms";
import { factorial, factorialBig, gcd, gcdBig, getAllFib, getAllFibBig, getDivisors, getDivisorsBig, getFactors, getFactorsBig, getFib, getFibBig, getNumPrimes, getNumPrimesBig, getPrimes, getPrimesBig, isPrime, isPrimeBig, sieveOfEratosthenes, sieveOfEratosthenesBig, sqrtBig, toBin, toBinBig } from "./NumberAlgorithms";


export function testSieveOfEratosthenes(){
	//Test 1
	let sieve = sieveOfEratosthenes();
	let correctAnswer: number[] = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97];
	let answer: number[] = [];
	for(let cnt = 0;cnt < 25;++cnt){
		let prime = sieve.next().value;
		if(typeof prime == "number"){
			answer.push(prime);
		}
	}
	assert.ok(arrayEquals(answer, correctAnswer), "sieveOfEratosthenes failed");

	//Test 2
	let bigSieve = sieveOfEratosthenesBig();
	let bigCorrectAnswer: bigint[] = [2n, 3n, 5n, 7n, 11n, 13n, 17n, 19n, 23n, 29n, 31n, 37n, 41n, 43n, 47n, 53n, 59n, 61n, 67n, 71n, 73n, 79n, 83n, 89n, 97n];
	let bigAnswer: bigint[] = [];
	for(let cnt = 0;cnt < 25;++cnt){
		let prime = bigSieve.next().value;
		if(typeof prime == "bigint"){
			bigAnswer.push(prime);
		}
	}
	assert.ok(arrayEquals(bigAnswer, bigCorrectAnswer), "sieveOfEratosthenes big failed");

	console.log("sieveOfEratosthenes passed");
}

export function testSqrtBig(){
	//Test 1
	let square: bigint = 25n;
	let correctAnswer: bigint = 5n;
	let answer = sqrtBig(square);
	assert.ok((answer == correctAnswer), "sqrtBig test 1 failed");

	//Test 2
	square = 0n;
	correctAnswer = 0n;
	answer = sqrtBig(square);
	assert.ok((answer == correctAnswer), "sqrtBig test 2 failed");

	console.log("testSqrtBig passed");
}

export function testGetAllFib(){
	//Test 1
	let correctAnswer = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89];
	let highestNumber = 100;
	let answer = getAllFib(highestNumber);
	assert.ok(arrayEquals(answer, correctAnswer), "getAllFib number 1 failed");

	//Test 2
	correctAnswer = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987]
	highestNumber = 1000;
	answer = getAllFib(highestNumber);
	assert.ok(arrayEquals(answer, correctAnswer), "getAllFib number 2 failed");

	//Test 3
	let bigCorrectAnswer = [1n, 1n, 2n, 3n, 5n, 8n, 13n, 21n, 34n, 55n, 89n];
	let bigHighestNumber = 100n;
	let bigAnswer = getAllFibBig(bigHighestNumber);
	assert.ok(arrayEquals(bigAnswer, bigCorrectAnswer), "getAllFibBig failed");

	console.log("testGetAllFib passed");
}

export function testGetPrimes(){
	//Test1
	let correctAnswer = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97];
	let topNum = 100;
	let answer = getPrimes(topNum);
	assert.ok(arrayEquals(answer, correctAnswer), "getPrimes number failed");

	//Test 2
	let bigCorrectAnswer = [2n, 3n, 5n, 7n, 11n, 13n, 17n, 19n, 23n, 29n, 31n, 37n, 41n, 43n, 47n, 53n, 59n, 61n, 67n, 71n, 73n, 79n, 83n, 89n, 97n];
	let bigTopNum = 100n;
	let bigAnswer = getPrimesBig(bigTopNum);
	assert.ok(arrayEquals(bigAnswer, bigCorrectAnswer), "getPrimesBig failed");

	console.log("getPrimes passed");
}

export function testGetNumPrimes(){
	//Test1
	let correctAnswer = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97];
	let numPrimes = 25;
	let answer = getNumPrimes(numPrimes);
	assert.ok(arrayEquals(answer, correctAnswer), "getNumPrimes number failed");

	//Test2
	let bigCorrectAnswer = [2n, 3n, 5n, 7n, 11n, 13n, 17n, 19n, 23n, 29n, 31n, 37n, 41n, 43n, 47n, 53n, 59n, 61n, 67n, 71n, 73n, 79n, 83n, 89n, 97n];
	let bigNumPrimes = 25n;
	let bigAnswer = getNumPrimesBig(bigNumPrimes);
	assert.ok(arrayEquals(bigAnswer, bigCorrectAnswer), "getNumPrimesBig failed");

	console.log("getNumPrimes passed");
}

export function testIsPrime(){
	//Test 1
	let num: number = 2;
	let correctAnswer: boolean = true;
	let answer: boolean = isPrime(num);
	assert.ok((answer == correctAnswer), "isPrime number 1 failed");
	//Test 2
	num = 97;
	correctAnswer = true;
	answer = isPrime(num);
	assert.ok((answer == correctAnswer), "isPrime number 2 failed");
	//Test 3
	num = 1000;
	correctAnswer = false;
	answer = isPrime(num);
	assert.ok((answer == correctAnswer), "isPrime number 3 failed");
	//Test 4
	num = 1;
	correctAnswer = false;
	answer = isPrime(num);
	assert.ok((answer == correctAnswer), "isPrime number 4 failed");

	//Test 5
	let bigNum: bigint = 2n;
	correctAnswer = true;
	answer = isPrimeBig(bigNum);
	assert.ok((answer == correctAnswer), "isPrime big 1 failed");
	//Test 6
	bigNum = 97n;
	correctAnswer = true;
	answer = isPrimeBig(bigNum);
	assert.ok((answer == correctAnswer), "isPrime big 2 failed");
	//Test 7
	bigNum = 1000n;
	correctAnswer = false;
	answer = isPrimeBig(bigNum);
	assert.ok((answer == correctAnswer), "isPrime big 3 failed");
	//Test 8
	bigNum = 1n;
	correctAnswer = false;
	answer = isPrimeBig(bigNum);
	assert.ok((answer == correctAnswer), "isPrime big 4 failed");

	console.log("isPrime passed");
}

export function testGetFactors(){
	//Test 1
	let correctAnswer = [2, 2, 5, 5];
	let number = 100;
	let answer = getFactors(number);
	assert.ok(arrayEquals(answer, correctAnswer), "getFactor number 1 failed");
	//Test 2
	correctAnswer = [2, 7, 7];
	number = 98;
	answer = getFactors(number);
	assert.ok(arrayEquals(answer, correctAnswer), "getFactor number 2 failed");

	//Test 3
	let bigCorrectAnswer = [2n, 7n, 7n];
	let bigNumber = 98n;
	let bigAnswer = getFactorsBig(bigNumber);
	assert.ok(arrayEquals(bigAnswer, bigCorrectAnswer), "getFactors BigInt failed");

	console.log("getFactors passed");
}

export function testGetDivisors(){
	//Test 1
	let correctAnswer: number[] = [1, 2, 4, 5, 10, 20, 25, 50, 100];
	let topNum: number = 100;
	let answer: number[] = getDivisors(topNum);
	assert.ok(arrayEquals(answer, correctAnswer), "getDivisors number failed");

	//Test 2
	let bigCorrectAnswer: bigint[] = [1n, 2n, 4n, 5n, 10n, 20n, 25n, 50n, 100n];
	let bigTopNum: bigint = 100n;
	let bigAnswer: bigint[] = getDivisorsBig(bigTopNum);
	assert.ok(arrayEquals(bigAnswer, bigCorrectAnswer), "getDivisors BigInt failed");

	console.log("getDivisors passed");
}

export function testGetFib(){
	//Test 1
	let correctAnswer: number = 144;
	let number: number = 12;
	let answer: number = getFib(number);
	assert.ok((answer == correctAnswer), "getFib number 1 failed");
	//Test 2
	correctAnswer = 6765;
	number = 20;
	answer = getFib(number);
	assert.ok((answer == correctAnswer), "getFib number 2 failed");

	//Test 3
	let bigCorrectAnswer: bigint = 144n;
	let bigNumber: bigint = 12n;
	let bigAnswer: bigint = getFibBig(bigNumber);
	assert.ok((bigAnswer == bigCorrectAnswer), "getFib BigInt 1 failed");
	//Test 4
	bigCorrectAnswer = 6765n;
	bigNumber = 20n;
	bigAnswer = getFibBig(bigNumber);
	assert.ok((bigAnswer == bigCorrectAnswer), "getFib BigInt 2 failed");
	//Test 5
	bigCorrectAnswer = 1070066266382758936764980584457396885083683896632151665013235203375314520604694040621889147582489792657804694888177591957484336466672569959512996030461262748092482186144069433051234774442750273781753087579391666192149259186759553966422837148943113074699503439547001985432609723067290192870526447243726117715821825548491120525013201478612965931381792235559657452039506137551467837543229119602129934048260706175397706847068202895486902666185435124521900369480641357447470911707619766945691070098024393439617474103736912503231365532164773697023167755051595173518460579954919410967778373229665796581646513903488154256310184224190259846088000110186255550245493937113651657039447629584714548523425950428582425306083544435428212611008992863795048006894330309773217834864543113205765659868456288616808718693835297350643986297640660000723562917905207051164077614812491885830945940566688339109350944456576357666151619317753792891661581327159616877487983821820492520348473874384736771934512787029218636250627816n;
	bigNumber = 4782n;
	bigAnswer = getFibBig(bigNumber);
	assert.ok((bigAnswer == bigCorrectAnswer), "getFib BigInt 3 failed");

	console.log("getFib passed");
}

export function testGCD(){
	//Test 1
	let num1: number = 2;
	let num2: number = 3;
	let correctAnswer: number = 1;
	let answer: number = gcd(num1, num2);
	assert.ok((answer == correctAnswer), "gcd number 1 failed");
	//Test 2
	num1 = 1000;
	num2 = 575;
	correctAnswer = 25;
	answer = gcd(num1, num2);
	assert.ok((answer == correctAnswer), "gcd number 2 failed");
	//Test 3
	num1 = 1000;
	num2 = 1000;
	correctAnswer = 1000;
	answer = gcd(num1, num2);
	assert.ok((answer == correctAnswer), "gcd number 3 failed");

	//Test 4
	let bigNum1: bigint = 2n;
	let bigNum2: bigint = 3n;
	let bigCorrectAnswer: bigint = 1n;
	let bigAnswer: bigint = gcdBig(bigNum1, bigNum2);
	assert.ok((bigAnswer == bigCorrectAnswer), "gcd BigInt 1 failed");
	//Test 5
	bigNum1 = 1000n;
	bigNum2 = 575n;
	bigCorrectAnswer = 25n;
	bigAnswer = gcdBig(bigNum1, bigNum2);
	assert.ok((bigAnswer == bigCorrectAnswer), "gcd BigInt 2 failed");
	//Test 6
	bigNum1 = 1000n;
	bigNum2 = 1000n;
	bigCorrectAnswer = 1000n;
	bigAnswer = gcdBig(bigNum1, bigNum2);
	assert.ok((bigAnswer == bigCorrectAnswer), "gcd BigInt 3 failed");

	console.log("getGCD passed");
}

export function testFactorial(){
	//Test 1
	let num: number = 1;
	let correctAnswer: number = 1;
	let answer: number = factorial(num);
	assert.ok((correctAnswer == answer), "factorial number 1 failed");
	//Test 2
	num = 10;
	correctAnswer = 3628800;
	answer = factorial(num);
	assert.ok((correctAnswer == answer), "factorial number 2 failed");
	//Test 3
	num = -5;
	correctAnswer = 1;
	answer = factorial(num);
	assert.ok((correctAnswer == answer), "factorial number 3 failed");

	//Test 4
	let numBig: bigint = 1n;
	let correctAnswerBig: bigint = 1n;
	let answerBig = factorialBig(numBig);
	assert.ok((correctAnswerBig == answerBig), "factorialBig number 1 failed");

	console.log("factorial passed");
}

export function testToBin(){
	//Test 1
	let num: number = 7;
	let correctAnswer: string = "111";
	let answer: string = toBin(num);
	assert.ok((correctAnswer == answer), "toBin number 1 failed");
	//Test 2
	num = 0;
	correctAnswer = "0";
	answer = toBin(num);
	assert.ok((correctAnswer == answer), "toBin number 2 failed");
	//Test 3
	num = 1000000;
	correctAnswer = "11110100001001000000";
	answer = toBin(num);
	assert.ok((correctAnswer == answer), "toBin number 3 failed");

	//Test 4
	let numBig: bigint = 7n;
	correctAnswer = "111";
	answer = toBinBig(numBig);
	assert.ok((correctAnswer == answer), "toBinBig number 1 failed");
	//Test 5
	numBig = 0n;
	correctAnswer = "0";
	answer = toBinBig(numBig);
	assert.ok((correctAnswer == answer), "toBinBig number 2 failed");
	//Test 6
	numBig = 1000000n;
	correctAnswer = "11110100001001000000";
	answer = toBinBig(numBig);
	assert.ok((correctAnswer == answer), "toBinBig number 3 failed");

	console.log("toBin passed");
}
