//typescriptClasses/TestStringAlgorithms.ts
//Matthew Ellison
// Created: 07-13-21
//Modified: 07-13-21
//Tests for the string algorithms
/*
Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import assert = require("assert");
import { arrayEquals } from "./ArrayAlgorithms";
import { getPermutations, isPalindrome } from "./StringAlgorithms";


export function testGetPermutations(){
	//Test 1
	let permString: string = "012";
	let correctAnswer: string[] = ["012", "021", "102", "120", "201", "210"];
	let answer: string[] = getPermutations(permString);
	assert.ok(arrayEquals(answer, correctAnswer), "getPermutations failed");

	console.log("getPermutations passed");
}

export function testIsPalindrome(){
	//Test 1
	let str: string = "101";
	let correctAnswer: boolean = true;
	let answer: boolean = isPalindrome(str);
	assert.ok((answer == correctAnswer), "isPalindrome number 1 failed");
	//Test 2
	str = "100";
	correctAnswer = false;
	answer = isPalindrome(str);
	assert.ok((answer == correctAnswer), "isPalindrome number 2 failed");
	//Test 3
	str = "";
	correctAnswer = true;
	answer = isPalindrome(str);
	assert.ok((answer == correctAnswer), "isPalindrome number 3 failed");

	console.log("isPalindrome passed");
}

export function testFindNumOccurrences(){
	//Test 1
	let str: string = "101";
	let correctAnswer: boolean = true;
	let answer = isPalindrome(str);
	assert.ok((answer == correctAnswer), "findNumOccurrences 1 failed");
	//Test 2
	str = "100";
	correctAnswer = false;
	answer = isPalindrome(str);
	assert.ok((answer == correctAnswer), "findNumOccurrences 2 failed");
	//Test 3
	str = "";
	correctAnswer = true;
	answer = isPalindrome(str);
	assert.ok((answer == correctAnswer), "findNumOccurrences 3 failed");

	console.log("findNumOccurrences passed");
}
