//typescriptClasses/ArrayAlgorithms.ts
//Matthew Ellison
// Created: 07-13-21
//Modified: 07-13-21
//Algorithms for arrays
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


//Returns true if array 1 and array 2 elements are the same
export function arrayEquals(array1: any[], array2: any[]): boolean{
	//If they aren't the same type they aren't equal
	if((typeof array1) != (typeof array2)){
		return false;
	}
	//If they aren't the same length they aren't equal
	else if(array1.length != array2.length){
		return false;
	}
	else{
		//Loop through every element to see if each one is equal
		for(let cnt = 0;cnt < array1.length;++cnt){
			//If any element in the same location is different return false
			if(array1[cnt] != array2[cnt]){
				return false;
			}
		}
		//If every element was the same they are equal
		return true;
	}
}

//Returns the sum of all elements in the array
export function getSum(ary: number[]): number{
	let sum: number = 0;
	ary.forEach(a => sum += a);
	return sum;
}
export function getSumBig(ary: bigint[]): bigint{
	let sum: bigint = 0n;
	ary.forEach(a => sum += a);
	return sum;
}

//Returns the product of all elements in the array
export function getProd(ary: number[]): number{
	//Return 0 if the array is empty
	if(ary.length == 0){
		return 0;
	}

	let prod: number = 1;
	ary.forEach(a => prod *= a);
	return prod;
}
export function getProdBig(ary: bigint[]): bigint{
	//Return 0 if the array is empty
	if(ary.length == 0){
		return 0n;
	}

	let prod: bigint = 1n;
	ary.forEach(a => prod *= a);
	return prod;
}
