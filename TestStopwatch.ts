//typescriptClasses/TestStopwatch.ts
//Matthew Ellison
// Created: 10-18-20
//Modified: 10-18-20
//This file contains a class that is used to time the execution time of other programs
/*
Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import assert = require("assert");
import {Stopwatch} from "./Stopwatch";


let NUM_TO_RUN = 100000;

function testStartStop(): void{
	let timer: Stopwatch = new Stopwatch();
	timer.start();
	timer.stop();
	//If it gets to here without throwing an exception everything went well
	console.log("start/stop passed");
}
function testConversion(): void{
	let timer: Stopwatch = new Stopwatch();
	let sum: number = 0;
	//Start the timer
	timer.start();
	//Do something to run some time
	for(let cnt: number = 0;cnt < NUM_TO_RUN;++cnt){
		sum += cnt;
	}
	//Stop the timer
	timer.stop();
	//Assert something so the sum isn't ignored during compile
	assert.notStrictEqual(sum, 0, "You really messed up");
	//Check that the different resolutions work out correctly
	let nano: number = timer.getNano();
	assert.strictEqual(timer.getMicro(), nano / 1000, "Micro resolution test failed");
	assert.strictEqual(timer.getMilli(), nano / 1000000, "Milli resolution test failed");
	assert.strictEqual(timer.getSecond(), nano / 1000000000, "Minute resolution test failed");
	assert.strictEqual(timer.getMinute(), nano / 60000000000, "Minute resolution test failed");
	assert.strictEqual(timer.getHour(), nano / 3600000000000, "Hour resolution test failed");
	console.log("conversion passed");
}
function testStringConversion(): void{
	//Test nanoseconds
	let results: string = Stopwatch.getStr(1.0);
	assert.strictEqual(results, "1.000 nanoseconds", "String conversion to nanoseconds failed");
	//Test microseconds
	results = Stopwatch.getStr(1.0e3);
	assert.strictEqual(results, "1.000 microseconds", "String conversion to microseconds failed");
	//Test milliseconds
	results = Stopwatch.getStr(1.0e6);
	assert.strictEqual(results, "1.000 milliseconds", "String conversion to milliseconds failed");
	//Test seconds
	results = Stopwatch.getStr(1.0e9);
	assert.strictEqual(results, "1.000 seconds", "String conversion to seconds failed");
	//Test mintues
	results = Stopwatch.getStr(1.0e12);
	assert.strictEqual(results, "16.667 minutes", "String conversion to minutes failed");
	//Rest hours
	results = Stopwatch.getStr(1.0e13);
	assert.strictEqual(results, "2.778 hours", "String conversion to hours failed");
	console.log("string conversion passed");
}

//Run the functions
try{
	testStartStop();
}
catch(error){
	console.error("start/stop test failed!");
}
try{
	testConversion();
}
catch(error){
	console.error("conversion test failed!");
}
try{
	testStringConversion();
}
catch(error){
	console.error("string conversion test failed!");
}
