//typescriptClasses/StringAlgorithms.ts
//Matthew Ellison
// Created: 07-13-21
//Modified: 07-13-21
//Algorithms for strings
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


//This is a function that creates all permutations of a string and returns a vector of those permutations
export function getPermutations(master: string): string[]{
	return getPermutationsHelper(master, 0);
}
function getPermutationsHelper(master: string, num: number): string[]{
	let perms: string[] = [];
	//Check if the number is out of bounds
	if((num >= master.length) || (num < 0)){
		//Do nothing and return an empty array
	}
	//If this is the last possible recurse just return the current string
	else if(num == (master.length - 1)){
		perms.push(master);
	}
	//If there are more possible recurses, recurse with the current permutation
	else{
		let temp: string[] = getPermutationsHelper(master, num + 1);
		temp.forEach(str => perms.push(str));
		//You need to swap the current letter with every possible letter after it
		//The ones needed to swap before will happen automatically when the function recurses
		for(let cnt = 1;(num + cnt) < master.length;++cnt){
			master = swapString(master, num, (num + cnt));
			temp = getPermutationsHelper(master, num + 1);
			temp.forEach(str => perms.push(str));
			master = swapString(master, num, (num + cnt));
		}

		//The array is not necessarily in alph-numeric order. So if this is the full array sort it before returning
		if(num == 0){
			perms.sort();
		}
	}

	//Return the array that was build
	return perms;
}
function swapString(str: string, first: number, second: number): string{
	let tempStr: string = str.substr(0, first) + str[second] + str.substr(first + 1, second - first - 1) + str[first] + str.substr(second + 1);

	return tempStr;
}

//Returns true if the string passed in is a palindrome
export function isPalindrome(str: string): boolean{
	let rev = str.split("").reverse().join("");
	if(str == rev){
		return true;
	}
	else{
		return false;
	}
}

//This function returns the number of times the character occurs in the string
export function findNumOccurrence(str: string, ch: string){
	let num: number = 0;	//Set the number of occurrences to 0 to start
	//Loop through every character in the string and compare it to the character passed in
	for(let strCh of str){
		//If the character is the same as the one passed in increment the counter
		if(strCh == ch){
			++num;
		}
	}
	//Return the number of times the character appeared in the string
	return num;
}