//typescriptClasses/TestAlgorithms.ts
//Matthew Ellison
// Created: 10-19-20
//Modified: 07-13-21
//This class holds many algorithms that I have found it useful to keep around
/*
Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import { testArrayEquals, testGetSum, testGetProd } from "./TestArrayAlgorithms";
import { testFactorial, testGCD, testGetAllFib, testGetDivisors, testGetFactors, testGetFib, testGetNumPrimes, testGetPrimes, testIsPrime, testSieveOfEratosthenes, testSqrtBig, testToBin } from "./TestNumberAlgorithms";
import { testFindNumOccurrences, testGetPermutations, testIsPalindrome } from "./TestStringAlgorithms";


//Run the array tests
testArrayEquals();
testGetSum();
testGetProd();

//Run the number tests
testSieveOfEratosthenes();
testSqrtBig();
testGetAllFib();
testGetPrimes();
testGetNumPrimes();
testIsPrime();
testGetFactors();
testGetDivisors();
testGetFib();
testGCD();
testFactorial();
testToBin();

//Run the string tests
testGetPermutations();
testIsPalindrome();
testFindNumOccurrences();

console.log("All tests passed");
