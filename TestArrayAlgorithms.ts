//typescriptClasses/TestArrayAlgorithms.ts
//Matthew Ellison
// Created: 07-13-21
//Modified: 07-13-21
//Tests for the array algorithms
/*
Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


import assert = require("assert");
import { arrayEquals, getProd, getProdBig, getSum, getSumBig } from "./ArrayAlgorithms";


export function testArrayEquals(){
	//Test 1
	let ary1: number[] = [];
	let ary2: number[] = [];
	let correctAnswer = true;
	let answer = arrayEquals(ary1, ary2);
	assert.ok((answer == correctAnswer), "arrayEquals test 1 failed");

	//Test 2
	ary1 = [1, 2, 3, 4, 5];
	ary2 = [1, 2, 3, 4, 5];
	correctAnswer = true;
	answer = arrayEquals(ary1, ary2);
	assert.ok((answer == correctAnswer), "arrayEquals test 2 failed");

	//Test 3
	ary1 = [1, 2, 3, 4, 5];
	ary2 = [1];
	correctAnswer = false;
	answer = arrayEquals(ary1, ary2);
	assert.ok((answer == correctAnswer), "arrayEquals test 3 failed");

	//Test 4
	ary1 = [1, 2, 3, 4, 5];
	ary2 = [6, 2, 3, 4, 5];
	correctAnswer = false;
	answer = arrayEquals(ary1, ary2);
	assert.ok((answer == correctAnswer), "arrayEquals test 4 failed");

	//Test 5
	ary1 = [1, 2, 3, 4, 5];
	ary2 = [1, 2, 3, 4, 6];
	correctAnswer = false;
	answer = arrayEquals(ary1, ary2);
	assert.ok((answer == correctAnswer), "arrayEquals test 5 failed");

	//Test 6
	let ary3: string[] = ["test1", "test2"];
	let ary4: string[] = ["test1", "test2"];
	correctAnswer = true;
	answer = arrayEquals(ary3, ary4);
	assert.ok((answer == correctAnswer), "arrayEquals test 6 failed");

	//Test 7
	ary3 = ["test1", "Test2"];
	ary4 = ["test1", "test2"];
	correctAnswer = false;
	answer = arrayEquals(ary3, ary4);
	assert.ok((answer == correctAnswer), "arrayEquals test 7 failed");

	console.log("testArrayEquals passed");
}

export function testGetSum(){
	//Test 1
	let correctAnswer: number = 0;
	let numbers: number[] = [];
	let answer: number = getSum(numbers);
	assert.ok((correctAnswer == answer), "getSum number 1 failed");
	//Test 2
	correctAnswer = 118;
	numbers = [2, 2, 3, 3, 4, 4, 100];
	answer = getSum(numbers);
	assert.ok((correctAnswer == answer), "getSum number 2 failed");

	//Test 3
	let bigCorrectAnswer: bigint = 118n;
	let bigNumbers: bigint[] = [2n, 2n, 3n, 3n, 4n, 4n, 100n];
	let bigAnswer: bigint = getSumBig(bigNumbers);
	assert.ok((bigCorrectAnswer == bigAnswer), "getSumBig failed");

	console.log("getSum passed");
}

export function testGetProd(){
	//Test 1
	let correctAnswer: number = 0;
	let numbers: number[] = [];
	let answer: number = getProd(numbers);
	assert.ok((correctAnswer == answer), "getProd number 1 failed");
	//Test 2
	correctAnswer = 57600;
	numbers = [2, 2, 3, 3, 4, 4, 100];
	answer = getProd(numbers);
	assert.ok((correctAnswer == answer), "getProd number 2 failed");

	//Test 3
	let bigCorrectAnswer: bigint = 57600n;
	let bigNumbers: bigint[] = [2n, 2n, 3n, 3n, 4n, 4n, 100n];
	let bigAnswer: bigint = getProdBig(bigNumbers);
	assert.ok((bigCorrectAnswer == bigAnswer), "getProdBig failed");

	console.log("getProd passed");
}
